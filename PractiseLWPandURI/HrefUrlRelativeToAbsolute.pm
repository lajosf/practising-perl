package HrefUrlRelativeToAbsolute;

use strict;
use warnings FATAL => 'all';
use LWP 5.64;
use URI;

sub new {
    my $class = shift;
    my $self = {
        _inputUrl => shift
        };
    bless $self, $class;
    return $self;
}

sub getHrefUrlsFromWebpageSource {
    my @input = @_;
    my $url = $input[1];
    # Letöltöm a tartalmát az oldalnak
    my $browser = LWP::UserAgent->new;
    my $response = $browser->get($url);
    die "Error at $url\n ", $response->status_line, "\n Aborting"
        unless $response->is_success;

    # Kiszedem a href-ekből az URL-eket
    # Lenti pattern egyszerűsítve leírva: .*?href="\(relativ URL találatok ami href-ben van)"
    # Ehhez hasonlókat kiszűri: href="http://... href="//... href="https://... href="ftp://...
    # Csak ilyen kezdetűeket tart meg: href="/urlstbstb...
    my $regexPattern = qr/.*?href=\"(\/[-a-zA-Z0-9]+[-a-zA-Z0-9+&@#\/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#\/%=~_|])\".*?/;
    return $response->decoded_content =~ /$regexPattern/g;
}

sub convertFromRelativeToAbsolutePath {
    my( $self ) = @_;
    my @resultList;
    my $uriUrl = URI->new($self->{_inputUrl});
    # A kinyert relatív path-okból csinálok abszolút path-t
    foreach $a (@_) {
        my $absolutePathUrl = URI->new_abs($a, $uriUrl->scheme() . '://' . $uriUrl->host());
        push(@resultList, $absolutePathUrl);
    }
    return @resultList;
}

1;
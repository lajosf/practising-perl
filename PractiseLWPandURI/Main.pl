#!/usr/bin/perl

# Feladatleírás:  Írjon programot, ami letölt egy html-t és kiszedi belőle az <a> tagek href-jeit.
# A relatív linkekből csináljon abszolútot. (use LWP, URI, mást ne nagyon useoljon)

# Futtatása: perl Main2.pl https://en.wikipedia.org/wiki/Joan_of_Arc

use strict;
use warnings FATAL => 'all';
use HrefUrlRelativeToAbsolute;

if ($ARGV[0]) {
    my $url = $ARGV[0];
    my $hrefUrlsRelativeToAbsolute = HrefUrlRelativeToAbsolute->new($url);
    my @relativeUrls = $hrefUrlsRelativeToAbsolute->getHrefUrlsFromWebpageSource($url);
    my @absoluteUrls = $hrefUrlsRelativeToAbsolute->convertFromRelativeToAbsolutePath(@relativeUrls);
    foreach $a (@absoluteUrls) {
        print "$a\n";
    }
}
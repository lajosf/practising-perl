# LineCounter.pm

package RomanNumeralsOperations;

use strict;
use warnings;

my %linesPerRomanNumerals = ('I' => 1, 'II' => 2, 'III' => 3, 'V' => 2, 'X' => 2, 'L' => 2, 'C' => 1, 'D' => 2, 'M' => 4);

sub new {
    my $class = shift;
    my $self = {};
    bless $self, $class;
    return $self;
}

sub findMaxNumberOfLinesNumbersInAList {
    if (scalar @_ == 1) {
        die "Empty list given as parameter - $!";
    }

    my $maxNumberOfLines = 0;
    my @resultArray = ("");

    foreach $a (@_) {
        my $numberOfLines = getNumberOfLinesForARomanNumber($a);
        if ($numberOfLines > $maxNumberOfLines) {
            $maxNumberOfLines = $numberOfLines;
            @resultArray = ();
            push(@resultArray, $a);
        }
        elsif ($numberOfLines == $maxNumberOfLines) {
            push(@resultArray, $a);
        }
    }
    push(@resultArray, $maxNumberOfLines);
    return \@resultArray;
}

sub getNumberOfLinesForARomanNumber {
    my $sum = 0;
    foreach $a (split //, "@_") {
        if (exists($linesPerRomanNumerals{$a})) {
            $sum = $sum + $linesPerRomanNumerals{$a};
        }
    }
    return $sum;
}

1;

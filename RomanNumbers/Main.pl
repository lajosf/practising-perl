use strict;
use warnings;
use RomanNumerals;
use RomanNumeralsOperations;

# Generate roman numbers from 1 to 1000
my $romanNumerals = RomanNumerals->new();
my $romanNumbersFrom1To1000 = $romanNumerals->generateFrom1To1000();

# Count number of lines per roman numbers int the list. Give back the max lines number(s).
my $romanNumeralsOperations = RomanNumeralsOperations->new();
my $result = $romanNumeralsOperations->findMaxNumberOfLinesNumbersInAList(@$romanNumbersFrom1To1000);
my $arraySize = scalar @$result;

# Print out the results
print("Maximum number of lines in the roman number(s): " . @$result[$arraySize - 1] . "\n");
print("The number(s):\n");
for ($b = 0; $b < $arraySize - 1; $b = $b + 1) {
  print(@$result[$b] . "\n");
}

# RomanNumerals.pm

package RomanNumerals;

use strict;
use warnings;

my %egyesek = ('0' => "", '1' => "I", '2' => "II", '3' => "III", '4' => "IV", '5' => "V", '6' => "VI", '7' => "VII", '8' => "VIII", '9' => "IX");
my %tizesek = ('0' => "", '1' => "X", '2' => "XX", '3' => "XXX", '4' => "XL", '5' => "L", '6' => "LX", '7' => "LXX", '8' => "LXXX", '9' => "XC");
my %szazasok = ('0' => "", '1' => "C", '2' => "CC", '3' => "CCC", '4' => "CD", '5' => "D", '6' => "DC", '7' => "DCC", '8' => "DCCC", '9' => "CM");

sub new {
   my $class = shift;
   my $self = { };
   bless $self, $class;
   return $self;
}

sub generateFrom1To1000 () {
    my @result;
    for($a = 1; $a < 1000; $a = $a + 1) {
        if (100 <= $a and $a < 1000) {
            my @karakterek = split //, "$a";
            my $romanNumeral = $szazasok{$karakterek[0]} . $tizesek{$karakterek[1]} . $egyesek{$karakterek[2]};
            push(@result, $romanNumeral);
        } elsif (10 <= $a and $a < 100) {
            my @karakterek = split //, "$a";
            my $romanNumeral = $tizesek{$karakterek[0]} . $egyesek{$karakterek[1]};
            push(@result, $romanNumeral);
        } else {
            push(@result, $egyesek{$a});
        }
    }
    push(@result, "M");
    return \@result;
}

1;
#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use File::Find;

# Feladatleírás: Írjon programot, ami egy adott könyvtárban (rekurzívan) megkeresi a legnagyobb file-t. (use File::Find)

# Example input: perl Main.pl /Users/lajosf/Documents/

if ($ARGV[0]) {
    {
        my $path = $ARGV[0];
        my $maxSize = 0;
        my $maxSizeFileName;

        find(\&wanted, $path);

        sub wanted {
            my $fileSize = -s $File::Find::name;
            if ($fileSize > $maxSize) {
                $maxSize = $fileSize;
                $maxSizeFileName = $File::Find::name;
            }
        }
        print "--------------- RESULT ---------------\n";
        print "Largest file name: ", $maxSizeFileName, " with size: ", $maxSize, " bytes\n";
        print "--------------------------------------\n";
    }
}
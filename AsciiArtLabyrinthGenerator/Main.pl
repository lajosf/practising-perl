#!/usr/bin/perl
use strict;
use warnings;
use Switch;
# Feladatleírás: 4. Írjon programot, ami ascii art labirintust generál.
# Megoldásra választott algoritmus depth-first (nagyon sok fajta algoritmus létezik mostanra)
# Megoldás és algoritmus lépései:
# 1. Létrehozunk egy páratlan hosszúságú és szélességű mátrixot user input alapján.
#   * A mátrixban 1-es a fal és 0-ás az út. Alapból minden mező 1-esre állítunk
# 2. Kiválasztunk egy random kezdőpontot, amely páratlan hosszúságú és magasságú
# 3. Bejáró rekurzív algoritmus (lehet rekurzív, vagy stack-es megoldás. Rekuzívat választottam.)
#   * Megkapja a random kezdő pontot
#   * Véletlenszerűen 4 irányba mozoghat (bal, jobb, fel, le) 2 mezőnként. Nem léphet útra és
#     nem léphet ki a mátrixból.
#   * Ha elakad, akkor vissza lépked a bejárt úton, amíg nem tud tovább lépni, vagy a legvégén
#   , amikor már bejárta az egész mátrixot nincs hova lépni. Ekkor lesz kész a labirintusunk
# 4. Visszaadjuk az eredményt grafikus formában a console-ra ascii art-ként

# Script futtatása:
# - perl Main.pl, vagy
# - perl Main.pl 9 13 (9, 13 a sor és oszlopok száma. Utóbbinak nagyobbnak kell lennie, mint 3 és páratlannak kell lennie)

{
    my @maze;
    my $mazeSorokSzama;
    my $mazeOszlopokSzama;

    sub generateTable {
        $mazeSorokSzama = shift;
        $mazeOszlopokSzama = shift;
        for (my $sor = 0; $sor < $mazeSorokSzama; $sor = $sor + 1) {
            my @sorArray;
            for (my $oszlop = 0; $oszlop < $mazeOszlopokSzama; $oszlop = $oszlop + 1) {
                push(@sorArray, 1);
            }
            push @maze, \@sorArray;
        }
    }

    sub constructMazeRecursive {
        my $directions = generateRandom4StepsDirection();
        my $s = $_[0];
        my $o = $_[1];

        foreach $a (@$directions) {
            switch($a) {
                # Felfele mozgás, ha lehetséges
                case 1 {
                    if ($s - 2 >= 0 and $maze[ $s - 2 ][$o] != 0) {
                        # Ha itt még nem jártunk
                        $maze[ $s - 1 ][$o] = 0;
                        $maze[ $s - 2 ][$o] = 0;
                        constructMazeRecursive($s - 2, $o); # Rekurzív hívás!
                    }
                }
                # Jobbra mozgás, ha lehetséges
                case 2 {
                    if ($o + 2 < $mazeOszlopokSzama and $maze[ $s ][$o + 2] != 0) {
                        # Ha itt még nem jártunk
                        $maze[ $s ][$o + 1] = 0;
                        $maze[ $s ][$o + 2] = 0;
                        constructMazeRecursive($s, $o + 2); # Rekurzív hívás!
                    }
                }
                # Lefele mozgás, ha lehetséges
                case 3 {
                    if ($s + 2 < $mazeSorokSzama and $maze[ $s + 2 ][$o] != 0) {
                        # Ha itt még nem jártunk
                        $maze[ $s + 1 ][$o] = 0;
                        $maze[ $s + 2 ][$o] = 0;
                        constructMazeRecursive($s + 2, $o); # Rekurzív hívás!
                    }
                }
                # Balra mozgás, ha lehetséges
                case 4 {
                    if ($o - 2 >= 0 and $maze[ $s ][$o - 2] != 0) {
                        # Ha itt még nem jártunk
                        $maze[ $s ][$o - 1] = 0;
                        $maze[ $s ][$o - 2] = 0;
                        constructMazeRecursive($s, $o - 2); # Rekurzív hívás!
                    }
                }
            }
        }
    }

    sub generateRandom4StepsDirection {
        my @directions = (1, 2, 3, 4);
        # Fisher-Yates shuffle algoritmus a tömb randomizáláshoz
        my $i;
        for ($i = @directions; --$i;) {
            my $j = int rand($i + 1);
            next if $i == $j; # ha épp aktuális index megeggyezik a generált számmal, akkor nincs mit swap-olni
            @directions[$i, $j] = @directions[$j, $i];
        }
        return \@directions;
    }

    sub printMaze {
        print "\n";
        foreach my $sor (@maze) {
            foreach my $item (@$sor) {
                if ($item == 1) {
                    print "# ";
                }
                else {
                    print "  ";
                }
            }
            print "\n";
        }
        print "\n";
    }

    my $x = $ARGV[0];
    my $y = $ARGV[1];
    if ($ARGV[0] > 3 and $ARGV[1] > 3 and $x % 2 == 1 and $y % 2 == 1) {
        generateTable($x, $y);
        my $randomStartingX;
        my $randomStartingY;
        if (int rand(1) == 0) {
            $randomStartingX = int rand($x - 1);
            $randomStartingY = 0;
        }
        else {
            $randomStartingX = 0;
            $randomStartingY = int rand($y - 1);
        }
        $maze[$randomStartingX][$randomStartingY] = 0;
        constructMazeRecursive($randomStartingX, $randomStartingY);
    }
    else {
        print "Figyelem! Nem páratlan vagy/és 3-nál nagyobb sor, valamint oszlopszám lett megadva a programnak.\n";
        print "Default (15, 15) sor és oszlopszámmal generáltam neked labirintust\n";
        generateTable(15, 15);
        my $randomStartingX;
        my $randomStartingY;
        if (int rand(1) == 0) {
            $randomStartingX = int rand(14);
            $randomStartingY = 0;
        }
        else {
            $randomStartingX = 0;
            $randomStartingY = int rand(14);
        }
        $maze[$randomStartingX][$randomStartingY] = 0;
        constructMazeRecursive($randomStartingX, $randomStartingY);
    }
    printMaze();
}
